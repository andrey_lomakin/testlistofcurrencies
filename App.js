import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import TableView from './src/containers/TableView'
import SideMenu from 'react-native-side-menu'
import Menu from './src/components/Menu'

const URL = 'http://phisix-api3.appspot.com/stocks.json'

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: [] 
    }
    this.getValue = this.getValue.bind(this)
  }

  componentDidMount(){
    this.getValue()
    this.timer = setInterval(this.getValue, 15000);
  }

  render() {
    const menu = <Menu getValue={this.getValue}/>;

    return (
      <SideMenu 
        menu={menu}
        menuPosition={'right'}
      >
        <TableView
          style={ styles.container }
          data={ this.state.value } 
        />
      </SideMenu>
    );
  }

  getValue() {
    fetch(URL)
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState( {
        value: responseJson.stock
      });
    })
    .catch((error) => {
    });
  }
}

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: 'red'
  },
});