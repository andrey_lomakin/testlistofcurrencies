import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';

export default class TableView extends Component  {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={[styles.cell, styles.cellname]}>{this.props.symbol} </Text>
        <Text style={styles.cell}>{this.props.price.amount.toFixed(2)}</Text>
        <Text style={styles.cell}>{this.props.volume}</Text>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: { 
    flex: 1, 
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  cellname: {
    textAlign: 'left'
  },
  cell: {
    flex: 1,
    height: 50,
    fontSize: 18,
    paddingTop: 16,
    paddingRight: 9,
    paddingLeft: 9,
    borderWidth: 0.5,
    textAlign: 'right'
  }
});