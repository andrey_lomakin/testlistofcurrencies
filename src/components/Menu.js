import React, { Component } from 'react';
import {
  Dimensions,
  StyleSheet,
  ScrollView,
  View,
  Button
} from 'react-native';

const window = Dimensions.get('window');

const styles = StyleSheet.create({
  menu: {
    flex: 1,
    width: window.width/2,
    height: window.height,
    padding: 20,
  },
});

export default class Menu extends Component{
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ScrollView 
        scrollsToTop={false} 
        style={styles.menu}
      >
        <Button
          onPress={()=>{
            this.props.getValue()
          }}
          title="Обновить"
          color="grey"
        />
      </ScrollView>
  )
  }
}