import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  FlatList
} from 'react-native';
import Row from '../components/Row'

export default class TableView extends Component  {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#F0F8FF'}}>
          <Text style={[styles.cell, styles.cellname]}> Название </Text>
          <Text style={styles.cell}> Цена </Text>
          <Text style={styles.cell}> Количество </Text>
        </View>
        <FlatList
          data={this.props.data}
          keyExtractor={(item)=>item.symbol}
          // ListHeaderComponent={ <Text style={styles.header}>DD</Text> }
          renderItem={({item}) => 
            <Row {...item}/>
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { 
    flex: 1, 
    padding: 8, 
    paddingTop: 16, 
    justifyContent: 'center',
    backgroundColor: '#FFF'
    },
  cellname: {
    ...this.cell,
    textAlign: 'left'
  },
  cell: {
    flex: 1,
    height: 30,
    fontSize: 14,
    paddingTop: 4,
    paddingRight: 9,
    paddingLeft: 9,
    borderWidth: 0.5,
    textAlign: 'right'
  }
});